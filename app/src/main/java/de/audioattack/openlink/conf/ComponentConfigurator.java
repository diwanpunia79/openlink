package de.audioattack.openlink.conf;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

/**
 * Created by low012 on 20.10.17.
 */

public class ComponentConfigurator {

    private static final String TAG = ComponentConfigurator.class.getSimpleName();

    private final Context myContext;
    private final PackageManager myPackageManager;

    public ComponentConfigurator(final Context context) {
        myContext = context.getApplicationContext();
        myPackageManager = myContext.getPackageManager();
    }


    public void setEnabled(final Class<? extends Activity> activityClass, final boolean enabled) {

        setEnabled(activityClass, getState(enabled));
    }

    private void setEnabled(final Class clazz, final int state) {

        Log.d(TAG, "Setting " + clazz + " to " + (state == PackageManager.COMPONENT_ENABLED_STATE_ENABLED ? "enabled" : "disabled"));

        myPackageManager.setComponentEnabledSetting(
                new ComponentName(myContext, clazz),
                state,
                PackageManager.DONT_KILL_APP);
    }

    private static int getState(final boolean enabled) {

        return enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
    }

}
